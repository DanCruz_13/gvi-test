import angular from 'angular';
const ngRoute = require('angular-route');
import routing from './main.routes';
const Isotope = require('isotope-layout');

export class MainController {
  $http;

  awesomeThings = [];
  newThing = '';

  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }

  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
      });
  }

  addThing() {
    if(this.newThing) {
      this.$http.post('/api/things', {
        name: this.newThing
      });
      this.newThing = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`);
  }
}

export default angular.module('testGviApp.main', [ngRoute])
  .config(routing)
  .controller('mainCtrl', ['$scope', '$http', function($scope, $http,) {
    


    $scope.users = [];
    $scope.user = {};
    $scope.newUser = {};
    $scope.userSelected = 0;
    $scope.user.firstName = "";
    $scope.user.lastName = "";
    $scope.user.profilePicture = "https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg";
    $scope.user.date = "1900";
    $scope.user.color = "White";
    $scope.user.fColor = {};

    $scope.filter1 = true;
    $scope.filter2 = true;


    $scope.image = {};
    
    $scope.getUsers = function(){
      $http.get('https://reqres.in/api/users?page=1')
        .then(function(res) {
          console.log(res.data.data);
          $scope.users = res.data.data;
          

          $http.get('https://reqres.in/api/users?page=2')
            .then(function(res) {
              console.log(res.data.data);
              res.data.data.forEach(element => {
                $scope.users.push(element);                
              });              

              $http.get('https://reqres.in/api/users?page=3')
                .then(function(res) {
                  console.log(res.data.data);
                  res.data.data.forEach(element => {
                    $scope.users.push(element);                
                  });              

                  $http.get('https://reqres.in/api/users?page=4')
                    .then(function(res) {
                      console.log(res.data.data);
                      res.data.data.forEach(element => {
                        $scope.users.push(element);                
                      });              
                      console.log($scope.users);
                    })
                    .catch(function(err) {
                      console.log(err);
                    })

                })
                .catch(function(err) {
                  console.log(err);
                })

            })
            .catch(function(err) {
              console.log(err);
            })

        })
        .catch(function(err) {
          console.log(err);
        })        
    }

    $scope.searchUser = function(){
      if($scope.userSelected > 0){

        Swal.fire({
          title: 'Searching for user',
          onBeforeOpen: () => {
            Swal.showLoading()          
          }
        })
  
        var id = $scope.userSelected;

        do{
          id -= 12;
        } while (id>12)


        $http.get('https://reqres.in/api/users/'+id)
          .then(function(res){
            console.log(res.data.data);
            $scope.user.firstName = res.data.data.first_name;
            $scope.user.lastName = res.data.data.last_name;
            $scope.user.profilePicture = res.data.data.avatar;
            $scope.user.id = res.data.data.id;
            console.log($scope.user);
  
            $http.get('https://reqres.in/api/unknown/'+id)
              .then(function(res){
                console.log(res.data.data);
                $scope.user.date = res.data.data.year;
                $scope.user.color = res.data.data.name;
                $scope.user.fColor = {
                  "background-color" : res.data.data.color,
                };
  
                Swal.close()
            })
            .catch(function(err){
              console.log("Error en la peticion del color");
              console.log(err);
              Swal.close()
              Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'The information could not be obtained. There\'s a problem with the API of reqres. ',                
              })
            });
    
  
          })
          .catch(function(err){
            console.log("Error en la peticion del usuario");
            console.log(err);
            Swal.close()
            Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'The information could not be obtained. There\'s a problem with the API of reqres. ',                
              })            
          });                  
      }            
    }

    $scope.addUser = function(){
      console.log($scope.user.firstName);
      $scope.newUser = {
        "name": $scope.user.firstName + ' ' + $scope.user.lastName, 
        "job": "leader"
      }

      $http.post('https://reqres.in/api/users', $scope.newUser)
        .then(function(res){
          console.log(res);
          Swal.fire(
            'Success',
            'The user '+$scope.newUser.name +' has been succesfully created',
            'success'
          )
        })
        .catch(function(err){
          console.log(err);
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'Something went wrong with the API',                
          })
        })
    }

    $scope.updateUser = function(){
      $scope.newUser = {
        "name": $scope.user.firstName + ' ' + $scope.user.lastName, 
        "job": "zion resident"
      }

      $http.patch('https://reqres.in/api/users/'+$scope.user.id, $scope.newUser)
        .then(function(res){
          console.log(res);
          Swal.fire(
            'Success',
            'The user '+$scope.newUser.name +' has been succesfully updated',
            'success'
          )
        })
        .catch(function(err){
          console.log(err);
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'Something went wrong with the API',                
          })
        })
    }


    $scope.showImage = function(src){
      $('#imageModal').modal();
      $scope.image = src;
    }

    $scope.showModal = function(element){
      console.log(element);
      console.log(element.src);      
      $('#imageModal').modal();
      $scope.image = element.src;
    }

    $scope.filterImage = function(data){
      switch (data){
        case 1: 
          $scope.filter1 = true;
          $scope.filter2 = true;
        break;
        case 2:
          $scope.filter1 = true;
          $scope.filter2 = false;
        break;
        case 3: 
          $scope.filter1 = false;
          $scope.filter2 = true;
          break;
      }
    }

    $scope.getUsers();
  }])
  .component('main', {
    template: require('./main.html'),
    controller: 'mainCtrl'
  })
  .name;
