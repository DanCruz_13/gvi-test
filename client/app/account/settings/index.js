'use strict';

import angular from 'angular';
import SettingsController from './settings.controller';

export default angular.module('testGviApp.settings', [])
  .controller('SettingsController', SettingsController)
  .name;
