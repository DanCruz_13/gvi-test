$(document).ready ( function () {

      $(window).scroll(function() {
          // checks if window is scrolled more than 500px, adds/removes solid class
          if($(this).scrollTop() > 500) { 
            
              $('.navbar').addClass('solid');
              $('.active-nav').addClass('solid');
              $('.logo').addClass('solid');
              $('.nav-link-menu').addClass('solid');
          } else {
            
              $('.navbar').removeClass('solid');
              $('.active-nav').removeClass('solid');
              $('.logo').removeClass('solid');              
              $('.nav-link-menu').removeClass('solid');              
          }
      });
      
      let mainNavLinks = document.querySelectorAll(".nav-link-menu");      
      

      window.addEventListener("scroll", event => {
        let fromTop = window.scrollY;
        mainNavLinks.forEach(link => {
          let section = document.querySelector(link.hash);
            
          if (
            section.offsetTop <= fromTop &&
            section.offsetTop + section.offsetHeight > fromTop
          ) {
            link.classList.add("active-nav");
          } else {
            link.classList.remove("active-nav");
          }
        });
      });



      $(".nav-link-menu").click(function() {
          // remove classes from all
          $(".nav-link-menu").removeClass("active-nav");
          // add class to the one we clicked
          $(this).addClass("active-nav");
         var id = $(this).attr('href');  
          $('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
      });


        
    });